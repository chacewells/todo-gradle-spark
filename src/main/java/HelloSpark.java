import static spark.Spark.get;

public class HelloSpark {
  public static void main(String[] args) {
    get("/hello", (req, res) -> "<h1>Hello Spark!</h1>");
  }
}
